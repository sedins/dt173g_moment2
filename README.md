# DT173G - MOMENT 2
___

## Syfte

Målet med detta moment har varit att automatisera uppgifter som
underlättar vid utveckling och produktionssättning av en enklare
webbplats. Detta för att slippa många manuella steg som utförs
ofta under utvecklingens gång.

## Installation

Använd följande kommando i ett kommandofönster för att klona projektet till lokal katalog
`git clone https://sedins@bitbucket.org/sedins/dt173g_moment2.git`

I målkatalogen körs sedan följande kommando för att installera nödvändiga paket
`npm install`

## Kommandon

**gulp watch** - Bygger webbplats i katalogen "dev", startar webbserver, uppdaterar filer och webbläsare vid förändringar av filer under projektkatalogen "app". Läs kommentarer i gulpfile.js för ingående 
information om de olika stegen som utförs vid exekvering.

**gulp build** - Bygger release av webbplats för användning i produktion. Resultat sparas i katalogen "dist".

## Paket

**gulp** - Verktyget som sköter automatiseringen
**gulp-sass** - Kompilerar källkod i SCSS-format till CSS
**gulp-inject** - Skapar referenser till JS- och CSS-filer i HTML-filer
**gulp-concat** - Slår ihop filer
**gulp-uglify** - Minifierar JS-filer
**gulp-clean-css** - Minifierar CSS-filer
**browser-sync** - Delar ut webbplats samt laddar om webbsidor vid förändringar

## Notering

Tillägget browser-sync kan få problem på datorer med Windows 10. Det kan avhjälpas genom att installera tillägget i global scope innan installation.

`npm install -g browser-sync`

Mer information angående problemet på följande länk
https://browsersync.io/docs#windows-users

___

*Stefan Hälldahl, 2018-09-09*